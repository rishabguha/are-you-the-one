# areyoutheone

A short Clojure program to analyze the number of potential matchings left after each round of Are You the One?. To run the executable, download the jar from <https://goo.gl/nePZdf> and run `java -jar areyoutheone-0.1.0-SNAPSHOT-standalone.jar`. Each season takes 5-10 min to run on my laptop. Note that Season 2 is not supported, as the additional number of combinations incurred by the eleventh girl makes the program run out of memory.
