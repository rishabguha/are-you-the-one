(ns areyoutheone.dtypes)

(defrecord Pair [^String boy ^String girl])
(defmulti pretty-string class)
(defmethod pretty-string Pair [pair] (str (:boy pair)
                                      " <-> "
                                      (:girl pair)))

(defmethod print-method Pair [pair ^java.io.Writer writer]
  (print-method (pretty-string pair) writer))

(defrecord Matching [pairs])
(defmethod pretty-string Matching [matching] (clojure.string/join ", " (map pretty-string (:pairs matching))))
  
(defmethod print-method Matching [matching ^java.io.Writer writer]
   (print-method (pretty-string matching) writer))

(defrecord TruthBoothResult [^Pair pair ^Boolean result])
(defrecord GuessResult [guessed-matching ^Integer num-correct])
(defrecord EpisodeOutput [tb-results ^GuessResult guess-result])
