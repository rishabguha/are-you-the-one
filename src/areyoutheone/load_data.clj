(ns areyoutheone.load-data)

(require 'areyoutheone.dtypes)
(refer 'areyoutheone.dtypes)

(require '[clojure-csv.core :as csv])
(require '[clojure.string :as str])
(require '[clojure.set])
(require '[clojure.java.io :as io] )

(defn make-matching [boys-list girls-list]
  (->Matching (set (map ->Pair boys-list girls-list))))

(defn get-guys [data]
  (do (if (not= (first (first data)) "Guys")
           (throw (Exception. "The top right cell needs to say 'Guys'")))
      ; need to drop the first and last rows to get the list of guys
         (map first (drop-last (nthrest data 1)))))

(defn get-girls [data]
  (let [num-weeks (- (count (first data)) 1)
        extract-girls-from-week (fn [n]
                                  (set (map (fn[d] (nth d n))
                                            (drop-last (nthrest data 1)))))
        girls-from-all-weeks (set (map extract-girls-from-week
                                       (range 1 (+ num-weeks 1))))]
    (do (if (= 1 (count girls-from-all-weeks))
          (first girls-from-all-weeks)
          (throw (Exception. "Different set of girls in different weeks"))))))

(defn week-num-to-name [week-num-string]
  (keyword (str "week" (.replaceAll week-num-string "/" "-"))))

(defn get-guess-from-col [data col-num]  
  (let [guess-string (nthrest (map (fn[row] (nth row col-num)) data) 1)
        guess-girls (drop-last guess-string)
        num-correct (read-string (last guess-string))
        guessed-matching (make-matching (get-guys data) guess-girls)]
    (->GuessResult guessed-matching num-correct )))

(defn get-guesses-and-weeks [data]
  (let [week-names (map week-num-to-name (nthrest (first data) 1))
        num-weeks (count week-names)
        guesses (map (fn[n] (get-guess-from-col data n)) (range 1 (+ num-weeks 1)))]
    {:guesses (zipmap week-names guesses)
     :weeks week-names}))

(defn parse-tb-outcome [outcome-string]
  (case outcome-string
    "Not A Match" false
    "Perfect Match" true
    (throw (Exception. "Malformed truth booth input"))))

(defn create-tb-pair [pair-string]
  ;; the wikipedia input for truth booth is "girl & boy" so we need to
  ;; reverse it to fit the boy-girl format of everything else
  (apply ->Pair (reverse (map str/trim (str/split pair-string #" & ")))))

(defn create-tb-result [row]
  (->TruthBoothResult (create-tb-pair (first row)) (parse-tb-outcome (last row))))

;; zipmap but handles duplicates (by returning a list)
(defn zippy [l1 l2]
  (apply merge-with concat (map (fn [a b]{a (list b)}) l1 l2)))

(defn get-truth-booths [data-in]
  ;; skip header row
  (let [data (nthrest data-in 1)
        week-names (map (fn [r] (week-num-to-name (nth r 1))) data)
        tb-results (map create-tb-result data)]
    (zippy week-names tb-results)))

(defn get-season-data [season-num]
  (let [guesses-and-weeks (get-guesses-and-weeks
                           (csv/parse-csv
                            (slurp (io/resource (str "s" season-num "guesses.csv")))))
        guesses (:guesses guesses-and-weeks)
        weeks (:weeks guesses-and-weeks)
        tbs (get-truth-booths
             (csv/parse-csv (slurp (io/resource (str "s" season-num "truthbooths.csv")))))
        ]
    {:data (merge-with ->EpisodeOutput tbs guesses)
     :weeks weeks}))
