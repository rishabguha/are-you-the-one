(ns areyoutheone.core
  (:gen-class))

(require 'areyoutheone.dtypes)
(refer 'areyoutheone.dtypes)

(require 'areyoutheone.load-data)
(refer 'areyoutheone.load-data)


(require '[clojure.math.combinatorics :as combo])
(require '[clojure-csv.core :as csv])
(require '[clojure.string :as str])
(require '[clojure.set])

(defn generate-permutations [boys girls]
  (let [num-boys (count boys)
        num-girls (count girls)
        extra-girls (- num-girls num-boys)
        boys-duped (map (fn[b] (concat boys b))
                          (combo/combinations boys extra-girls))
        girls-permuted (combo/permutations girls)
        potentials (combo/cartesian-product boys-duped girls-permuted)
        permutations (map (fn [t] (apply make-matching t)) potentials)]
    permutations))
        
       

(defn check-truth-booth [potential-matchings truth-booth-result]
  (let [check-tb-consistency (fn [candidate-match]
                               (let [result-with-candidate
                                     (contains? (:pairs candidate-match)
                                                (:pair truth-booth-result))]
                                 (= result-with-candidate
                                    (:result truth-booth-result))))
        filtered-matches (filter check-tb-consistency potential-matchings)]
  (do
    (println (str "Pair tested at Truth Booth: "
                  (pretty-string (:pair truth-booth-result))))
    (println (str "Result: " (str/upper-case (:result truth-booth-result))))
    (flush)
    (println (str "Potential matches remaining: " (count filtered-matches)))
    filtered-matches)))


(defn check-guess [potential-matchings guess-result]
  (let [check-guess-consistency (fn [candidate-match]
                                  (= (:num-correct guess-result)
                                     (count (clojure.set/intersection
                                             (:pairs candidate-match)
                                             (:pairs (:guessed-matching guess-result))
                                             ))))
        filtered-matches (filter check-guess-consistency potential-matchings)]
    (do
      (println (str "Matching tested: "
                    (pretty-string (:guessed-matching guess-result))))
      (println (str "Number of lights: " (:num-correct guess-result)))
      (flush)
      (println (str "Potential matches remaining " (count filtered-matches)))
      (flush)
      filtered-matches)))

(defn initialize-analysis [season-data]
  (let [extract-pairs
        (fn [w]
          (:pairs (:guessed-matching (:guess-result (w (:data season-data))))))
        all-pairs
        (apply clojure.set/union (map extract-pairs (:weeks season-data)))
        ;; filter out strings with &
        filter-multiples (fn [s] (filter (fn [t] (not (.contains t "&"))) s))
        boys (filter-multiples (set (map (fn [t] (:boy t)) all-pairs)))
        girls (filter-multiples (set (map (fn [t] (:girl t)) all-pairs)))
        permutations (generate-permutations boys girls)]
    permutations))
    
(defn run-analysis [season-data]        
  (let  [make-week-analyzer
         (fn [week]
           (let [data (week (:data season-data))
                 tb-results (:tb-results data)
                 ;; function that takes a truth-booth result and specializes the checker
                 ;; to check that result against a potential matching
                 specialize-tb-checker (fn[tb] (fn [pm] (check-truth-booth pm tb)))
                 ;; we need to reverse the tb results because functions are composed
                 ;; right-to-left. tb-checker is a function that takes a potential matching
                 tb-checker (apply comp (reverse (map specialize-tb-checker tb-results)))
                 guess (:guess-result data)
                 guess-checker (fn[pm] (check-guess pm guess))]
             (fn [potential-matching] 
               (do
                 (println "Week: " (.replaceAll (str week) "[:a-z]" ""))
                 ((comp guess-checker tb-checker) potential-matching)))))
         week-analyzers (map make-week-analyzer (:weeks season-data))
         analyzer-func (apply comp (reverse week-analyzers))]
    (do
      (println "Starting")
      (analyzer-func (initialize-analysis season-data))
      (println "Finished"))))
        
(defn -main [& arg]
  (let [season-num (if arg (str (first arg)) (do (println "Season number?") (read-line)))
        season-data (get-season-data season-num)]
    (run-analysis season-data)))
